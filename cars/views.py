from django.shortcuts import render
from cars.scraper import get_data

URL = 'https://losangeles.craigslist.org/search/sss?query=miata&min_price=&max_price='

def show_cars(request):
    cars = get_data(URL)
    context = {"cars": cars}
    return render(request, "cars/list.html", context)
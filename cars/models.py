from django.db import models

class Car(models.Model):
    title = models.TextField()
    price = models.IntegerField()
    url = models.URLField(max_length=500)
